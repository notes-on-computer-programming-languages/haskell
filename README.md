# haskell

Purely functional. https://haskell.org

[[_TOC_]]

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=gcc%20rustc%20ocaml-base-nox%20ghc&show_installed=on&want_percent=1&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)
![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=ghc%20julia%20scala%20racket%20elixir%20clojure%20chezscheme&show_installed=on&want_percent=1&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)

# Structured set of materials
* [*An Opinionated Comparison of Software Design Approaches in Haskell*
  ](https://gist.github.com/graninas/1b7961ccaedf7b5cb92417a1599fdc99#file-haskell_approaches_comparison_table-md)
  (2024-07) Alexander Granin (graninas)
* [*Haskeller Competency Matrix*
  ](https://gist.github.com/graninas/833a9ff306338aefec7e543100c16ea1)
  (2022-07) Alexander Granin (graninas)

# Annual survey
* https://taylor.fausak.me
* [*2021 State of Haskell Survey Results*
  ](https://taylor.fausak.me/2021/11/16/haskell-survey-results/)

# Official documentation
* [Try Haskell](http://tryhaskell.org/)
* [*Meta-tutorial*](https://wiki.haskell.org/Meta-tutorial) (2012) wiki.haskell.org

## Optimization and Performance
* [*Haskell Optimization Handbook*
  ](https://haskell.foundation/hs-opt-handbook.github.io)
  (2024) Jeffrey M. Young (Input Output Global and Haskell Foundation)
* [*Performance*
  ](https://wiki.haskell.org/index.php?title=Performance)
  (2018-08) (HaskellWiki)

# Books
* [Haskell (Computer program language)
  ](https://www.worldcat.org/search?q=su%3AHaskell+%28Computer+program+language%29)
* [haskell](https://worldcat.org/search?q=haskell) Restrict search to 'Computer Science'.
---
Pre-publishing:
* [*Functional Design and Architecture*
  ](https://www.manning.com/books/functional-design-and-architecture)
  (2024?) Alexander Granin (graninas) (Manning)
  * https://graninas.com/functional-design-and-architecture-book/
  * https://github.com/graninas/Functional-Design-and-Architecture
* [*Software Design and Architecture in Haskell*
  ](https://github.com/graninas/software-design-in-haskell)
  (2021-2024?) Alexander Granin (graninas)
* [*Haskell Optimization Handbook*
  ](https://haskell.foundation/hs-opt-handbook.github.io/)
  (2022-2024-...) Jeffrey M. Young and contributors

---
* [*Effective Haskell: Solving Real-World Problems with Strongly Typed Functional Programming*
  ](https://pragprog.com/titles/rshaskell/effective-haskell/)
  2023-08 Rebecca Skinner
* [*Functional Declarative Design*
  ](https://github.com/graninas/functional-declarative-design-methodology)
  2023-08 Alexander Granin (GitHub) [Article]
* *Practical Haskell: A Real-World Guide to Functional Programming*
  2022-09 (3rd ed.) Alejandro Serrano Mena (Apress)
* [*Haskell (Almost) Standard Libraries*
  ]()
  (2022-03) Alejandro Serrano Mena
  * https://leanpub.com/haskell-stdlibs
* [*Functional Declarative Design: a Counterpart to Object-Oriented Design*
  ](https://www.oreilly.com/library/view/functional-declarative-design/10000MNHV2021137/)
  2021-09 Alexander Granin (Manning Publications) [Video]
* [*Haskell in Depth*
  ]()
  2021-05 Vitaly Bragilevsky (Manning)
* [*The Simple Haskell Handbook: Learn how to build Haskell applications*](https://leanpub.com/simple-haskell-book)
  2021-05 Marco Sampellegrini (Leanpub)
* [*Production Haskell*
  ](https://leanpub.com/production-haskell)
  Succeeding in Industry with Haskell
  (2021-02 .. 2023-02) Matt Parsons
  * [*Book Review: Production Haskell*
    ](https://dev.to/zelenya/book-review-production-haskell-17nm)
    2023-02 Zelenya
* [*Functional Design and Architecture*
  ](https://leanpub.com/functional-design-and-architecture)
  Building real programs in Haskell: application architectures, design patterns, best practices and approaches
  2020-10 Alexander Granin
* [*What I Wish I Knew When Learning Haskell*
  ](http://dev.stephendiehl.com/hask/)
  2020 Stephen Diehl
* *Real World Haskell*
  * [*Real World Haskell (update of the book)*
    ](https://github.com/tssm/up-to-date-real-world-haskell)
    (2020-08) tssm
  * [*Real World Haskell*
    ](http://book.realworldhaskell.org/read/)
    2008-11 by Bryan O'Sullivan, Don Stewart, and John Goerzen
* [*Haskell Quick Syntax Reference A Pocket Guide to the Language, APIs, and Library*
  ](https://www.worldcat.org/search?q=ti%3AHaskell+Quick+Syntax+Reference+A+Pocket+Guide+to+the+Language%2C+APIs%2C+and+Library&qt=advanced&dblist=638)
  2019 Stefania Loredana Nita; Marius Mihailescu
* *Practical Haskell: A Real World Guide to Programming*
  2019 (2nd ed.) Alejandro Serrano Mena
* *Practical Web Development with Haskell: Master the Essential Skills to Build Fast and Scalable Web Applications*
  2018-11 Ecky Putrady (Apress, Springer)
* *Getting Started with Haskell Data Analysis*
  2018-10 James Church (Packt)
* *Get Programming with Haskell*
  2018-03 Will Kurt (Manning)
  * [Will Kurt site:github.com](https://google.com/search?q=Will+Kurt+site:github.com)
  * https://github.com/Rhywun/get-programming-with-haskell
* [*Functional Programming: A PragPub Anthology: Exploring Clojure, Elixir, Haskell, Scala, and Swift*
  ](https://pragprog.com/titles/ppanth/functional-programming-a-pragpub-anthology/)
  2017-07 Michael Swaine and the PragPub writers
* *Purely functional data structures*
  1998 Chris Okasaki

# Advocacy
* There is a section on comparisons with other languages! (And this topic is also close to tutorial)
* [*What Makes Haskell Unique*
  ](https://www.snoyman.com/blog/2017/12/what-makes-haskell-unique/)
  2017-12 Michael Snoyman
* [*Is Haskell suitable for commercial software development?*
  ](https://www.quora.com/Is-Haskell-suitable-for-commercial-software-development)
  (2017) Quora

# Tutorials
* [*Learn X in Y minutes, where X=Haskell*](https://learnxinyminutes.com/docs/haskell/)
* [*Monday Morning Haskell*
  ](https://mmhaskell.com)
* [*Haskell Tutorials*
  ](https://www.haskelltutorials.com)
* [*Welcome to all those learning Haskell*
  ](https://williamyaoh.com)
---
* [*Getting Started with Haskell*
  ](http://mmhaskell.com/start-here)
  (2022) Monday Morning Haskell
* [*Monads (and other Functional Structures)*
  ](https://mmhaskell.com/monads)
  (2022) Monday Morning Haskell
* [*Haskell for python developers*
  ](https://www.softwarefactory-project.io/haskell-for-python-developers.html)
  2020-07 tristanC
* [*Descending sort in Haskell*
  ](https://ro-che.info/articles/2016-04-02-descending-sort-haskell)
  2020-04 2016-04 Roman Cheplyaka
* [*Monads in Functional Programming: a Practical Note*
  ](https://medium.com/@bobbypriambodo/monads-in-functional-programming-a-practical-note-53488f94b20c)
  2016-12 Bobby Priambodo
* [*Functors, Applicatives, And Monads In Pictures*
  ](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html)
  2013
* [*Haskell Cheat Sheet*
  ](https://hackage.haskell.org/package/CheatSheet-2.9/src/CheatSheet.pdf)
  2013 Justin Bailey
  * [CheatSheet](https://hackage.haskell.org/package/CheatSheet) @Hackage

# Limitations
## Haskell: The Bad Parts
* [*Haskell: The Bad Parts, part 1*
  ](https://www.snoyman.com/blog/2020/10/haskell-bad-parts-1/)
  2020-10 Michael Snoyman
  * foldl
  * sum/product
  * Data.Text.IO
  * Control.Exception.bracket
* [*Haskell: The Bad Parts, part 2*
  ](https://www.snoyman.com/blog/2020/11/haskell-bad-parts-2/)
  2020-11 Michael Snoyman
  * Partial functions (in general)
  * Values are partial too!
  * But ackshualllly, infinite loops
  * Hubris (project design is also important)
  * More partial functions!
  * Law-abiding type classes
  * Unused import warnings
  * The vector package
* [*Haskell: The Bad Parts, part 3*
  ](https://www.snoyman.com/blog/2020/12/haskell-bad-parts-3/)
  2020-12 Michael Snoyman
  * Pattern matching
  * Slow compilation
  * Text
  * Lazy data structures
  * Qualified imports
  * Should we fix this?

# Features
## Data Types
* [*Generate PureScript Data Types From Haskell Data Types*
  ](https://medium.com/swlh/generate-purescript-data-types-from-haskell-data-types-a8568254a37c)
  2021-01 Ong Yi Ren

### Accessor functions for the record's fields
* [*Working around Haskell's namespace problem for records*
  ](https://gist.github.com/mtesseract/1b69087b0aeeb6ddd7023ff05f7b7e68)
  2018 mtesseract

### Algebraic Data Types (ADT)
### Generalized Algebraic Data Types (GADT)
* [*Generalized algebraic data type*](https://en.m.wikipedia.org/wiki/Generalized_algebraic_data_type)
* [haskell gadt](https://google.com/search?q=haskell+gadt)
* Glasgow Haskell Compiler User's Guide
  * [*GHC Language Features
    *](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/glasgow_exts.html?highlight=gadt)
* Haskell/[*GADT*](https://en.wikibooks.org/wiki/Haskell/GADT)

### Iterators and Streams
* [*Iterators and Streams in Rust and Haskell*
  ](https://www.fpcomplete.com/blog/2017/07/iterators-streams-rust-haskell/)
  2017-07 Michael Snoyman

## Do notation
* [*do notation*](https://en.wikibooks.org/wiki/Haskell/do_notation)
  (2020) wikibooks ... Haskell
* [*Introduction to IO*](https://wiki.haskell.org/Introduction_to_IO)
  wiki.haskell
* [*Should do-notation be avoided in Haskell?*
  ](https://stackoverflow.com/questions/16726659/should-do-notation-be-avoided-in-haskell)
  (2016) stackoverflow
* [*How to write without Do notation*
  ](https://stackoverflow.com/questions/7229518/how-to-write-without-do-notation)
  (2013) stackoverflow

## Lazy, strict, eager evaluation
* The ! means "strict" – i.e. don't store a thunk for an Int here, but evaluate it first.
* ~
* [*Bang patterns and Strict Haskell*
  ](https://ghc.gitlab.haskell.org/ghc/doc/users_guide/exts/strict.html)
* [*What I Wish I Knew When Learning Haskell*
  ](http://dev.stephendiehl.com/hask/)
  2009-2020 Stephen Diehl
* [*Is it possible to force Haskell to use eager evaluation by default?*
  ](https://www.quora.com/Is-it-possible-to-force-Haskell-to-use-eager-evaluation-by-default)
  (2018)
  * [Chaddaï Fouché](https://www.quora.com/Is-it-possible-to-force-Haskell-to-use-eager-evaluation-by-default/answer/Chadda%C3%AF-Fouch%C3%A9)
* [*All About Strictness*
  ](https://www.fpcomplete.com/blog/2017/09/all-about-strictness/)
  2017-09 Michael Snoyman
  * Also: https://tech.fpcomplete.com/haskell/tutorial/all-about-strictness/
* [*Haskell Can Now Do Strict Evaluation by Default*
  ](https://www.infoq.com/news/2015/11/haskell-strict-eval-patch/)
  2015-11 Sergio De Simone
---
* [*Lazy vs. non-strict*
  ](https://wiki.haskell.org/Lazy_vs._non-strict)
  (2021) @HaskellWiki
* [*Performance/Strictness*
  ](https://wiki.haskell.org/Performance/Strictness)
  (2022-06) @HaskellWiki

## Error handling
* [*Maybe Haskell*
  ](https://gumroad.com/l/maybe-haskell)
  2015-02 Pat Brisbin [Book]

## Parallel and concurrent programming
* *Parallel and concurrent programming in Haskell*
  Marlow, Simon
  O'Reilly 2013
  * https://simonmar.github.io/pages/pcph.html
  * https://www.oreilly.com
  * worldcat.org
* Parallel and Concurrent Programming in Haskell

### Software Transactional Memory
* [stm: Software Transactional Memory](http://hackage.haskell.org/package/stm)

## Memory management

# Unofficial documentation
* [*Haskell Fan Site*
  ](https://crypto.stanford.edu/~blynn/haskell/)

## Wikis
* [*Haskell*
  ](https://en.wikibooks.org/wiki/Haskell)
  @ Wikibooks
* [*Write Yourself a Scheme in 48 Hours*
  ](https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours)

## Configuration and Reader Monad
* [*The Reader Monad — Part 1*
  ](https://hackernoon.com/the-reader-monad-part-1-1e4d947983a8)
  2017-06 Jonathan Fischoff
* [*Reader Monad Part 2*
  ](https://medium.com/@jonathangfischoff/monad-reader-part-2-d812dda1d03e)
  2017-07 Jonathan Fischoff

## Compare Haskell to other languages
* [*Haskell's Children*
  ](https://owenlynch.org/posts/2020-09-16-haskells-children/)
  2020-09 Owen Lynch
  * Rust, Idris, Julia ([algebraicjulia.org](https://algebraicjulia.org))
  * Not mentioning: Scala ([cat](https://index.scala-lang.org/typelevel/cats/cats-core)), Elixir ([witchcraft](https://hex.pm/packages/witchcraft)), [...](https://en.m.wikipedia.org/wiki/Haskell_(programming_language))
* [*Get Ready for Rust!*
  ](https://mmhaskell.com/blog/2019/11/18/get-ready-for-rust)
  2019-11 Monday Morning Haskell
  * "Rust is a very interesting language to compare to Haskell."
* [*What are the worst parts about using Haskell?*
  ](https://www.quora.com/What-are-the-worst-parts-about-using-Haskell/answer/Tikhon-Jelvis)
  (2019) Tikhon Jelvis
* [*10 things Idris improved over Haskell*
  ](https://deque.blog/2017/06/14/10-things-idris-improved-over-haskell/)
  2017-06 @ Deque

## Design Patterns and Dependent Type (to split)
* See also below [Dependency injection](#dependency-injection)
* [*Dependent type*
  ](https://wiki.haskell.org/Dependent_type)
  (2021-04) Haskell Wiki
---
* [*Serokell’s Work on GHC: Dependent Types*
  ](https://serokell.io/blog/ghc-dependent-types-in-haskell)
  2023-09 Serokell GHC team
* [*Dependent Types in Haskell*
  ](https://functional.works-hub.com/learn/Dependent-Types-in-Haskell-2)
  2021-08 Sasa Bogicevic
  * See also 2017-11
* [*Parse, don’t validate: the essence of type-driven design*
  ](https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/)
  2019-11 Alexis King
* [*Why Dependent Haskell is the Future of Software Development*
  ](https://serokell.io/blog/why-dependent-haskell)
  2018-12 Vladislav Zavialov
* [*Keep your types small... … and your bugs smaller*
  ](https://www.parsonsmatt.org/2018/10/02/small_types.html)
  2018-10 parsonsmatt
* [*Three Layer Haskell Cake*
  ](https://www.parsonsmatt.org/2018/03/22/three_layer_haskell_cake.html)
  2018-03 Matt Parsons
* [*Dependent Types in Haskell*
  ](https://medium.com/@Webmarmun/dependent-types-in-haskell-f35b8880cc16)
  2017-11 Bogicevic Sasa
  * See also 2021-08
* [*Type Safety Back and Forth*
  ](https://www.parsonsmatt.org/2017/10/11/type_safety_back_and_forth.html)
  2017-10 parsonsmatt
* [*Basic Type Level Programming in Haskell*
  ](https://www.parsonsmatt.org/2017/04/26/basic_type_level_programming_in_haskell.html)
  2017-04 parsonsmatt
* [*Design Patterns in Haskell*
  ](http://blog.ezyang.com/2010/05/design-patterns-in-haskel/)
  2010-05 Edward Z. Yang
---
* [*Haskell, AI, and Dependent Types I*
  ](https://mmhaskell.com/machine-learning/dependent-types)

## From OOP to Functional Programming
* [haskell for object oriented programmers
  ](https://google.com/search?q=haskell+for+object+oriented+programmers)
* [*Switching from OOP to Functional Programming*
  ](https://medium.com/@olxc/switching-from-oop-to-functional-programming-4187698d4d3)
  2019-01 Alexey Avramenko
* [*From design patterns to category theory*
  ](https://blog.ploeh.dk/2017/10/04/from-design-patterns-to-category-theory/)
  2017-10 Mark Seemann (ploeh blog)
---
  * [*Beyond OOP: Functional Programming with C#*
    ](https://slidle.com/jivko/functional-programming/intro)
    Jivko Petiov

### Dependency injection
* [haskell dependecy injection
  ](https://google.com/search?q=haskell+dependecy+injection)
* [haskell way of doing dependency injection
  ](https://google.com/search?q=haskell+way+of+doing+dependency+injection)

## Optimization and Performance
* [*Making Haskell programs faster and smaller*
  ](https://users.aber.ac.uk/afc/stricthaskell.html)
  2008? afc

### Books
* Production Haskell
* Haskell Optimization Handbook

## Parser combinators
### Monadic parser combinators
* [*Haskell/Monadic parser combinators*
  ](https://en.wikibooks.org/wiki/Haskell/Monadic_parser_combinators)
* (pdf) [*Monadic parser combinators*
  ](http://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf)
  1996 Hutton and Meijer

### DOM
* ghcjs-dom
  * [*Creating a Haskell Application Using Reflex. Part 3*
    ](https://typeable.io/blog/2021-05-24-reflex-3.html)
    2021-05 Nikita Anisimov (Typeable)

### List-based parser combinators
* [*List-based parser combinators in Haskell and Raku*
  ](https://wimvanderbauwhede.github.io/articles/list-based-parser-combinators/)
  2020-06 Wim Vanderbauwhede

### Application to text parsing
* [monadic parser combinator markdown](https://google.com/search?q=monadic+parser+combinator+markdown)


# Libraries
* [*State of the Haskell ecosystem*
  ](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md)
  (2020)
* Compare with Idris...

## Popularity of some Haskell libraries in Debian
![Popularity of some Haskell libraries in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=ghc+libghc-random-dev+libghc-hashable-dev+libghc-async-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Popularity of some Haskell libraries in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-async-dev+libghc-attoparsec-dev+libghc-network-dev+libghc-case-insensitive-dev+libghc-streaming-commons-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Popularity of some Haskell libraries in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-streaming-commons-dev+libghc-base64-bytestring-dev+libghc-basement-dev+libghc-http-types-dev+libghc-sha-dev+libghc-clock-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![Popularity of some Haskell libraries in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-clock-dev+libghc-entropy-dev+libghc-vault-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

## Base libraries, vector-like types and Prelude(s)
* [*Haskell base proposal: unifying vector-like types*
  ](https://www.snoyman.com/blog/2021/03/haskell-base-proposal/)
  2021-03 snoyberg

### Prelude
* See book: What I Wish I Knew When Learning Haskell (Version 2.5) Ref. in book section.

#### Popularity of some preludes in Debian
![Popularity of some preludes in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-safe-dev%20libghc-basement-dev%20libghc-foundation-dev%20libghc-rio-dev%20libghc-basic-prelude-dev%20libghc-classy-prelude-dev%20libghc-base-prelude-dev&show_installed=on&want_percent=1&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)
![Popularity of some preludes in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-rio-dev%20libghc-basic-prelude-dev%20libghc-classy-prelude-dev%20libghc-base-prelude-dev&show_installed=on&want_percent=1&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)
Number of dependencies
* 1 [safe](https://hackage.haskell.org/package/safe)
* 3 [basement](https://hackage.haskell.org/package/basement)
* 4  [foundation](https://hackage.haskell.org/package/foundation)
* 22 [rio](https://hackage.haskell.org/package/rio)
* 9 [basic-prelude](https://hackage.haskell.org/package/basic-prelude)
* 27 [classy-prelude](https://hackage.haskell.org/package/classy-prelude)
* 1 [base-prelude](https://hackage.haskell.org/package/base-prelude)

[Packages tagged prelude](https://hackage.haskell.org/packages/tag/prelude)

* 11 [relude](http://hackage.haskell.org/package/relude)

## List-based parser combinators
* [wimvanderbauwhede/list-based-combinators-hs](https://github.com/wimvanderbauwhede/list-based-combinators-hs)

## Monadic parser combinators
### [megaparsec](http://hackage.haskell.org/package/megaparsec)
* [*Megaparsec tutorial from IH book*
  ](https://markkarpov.com/megaparsec/megaparsec.html)
  2019-02 Mark Karpov

## Monadic parser combinators used to parse text

### [mmark-md/mmark](https://github.com/mmark-md/mmark)

### [pandoc: Conversion between markup formats](http://hackage.haskell.org/package/pandoc)

#### [Text.Pandoc.Readers.Markdown](http://hackage.haskell.org/package/pandoc-2.7.3/docs/Text-Pandoc-Readers-Markdown.html)

#### [Text.Pandoc.Writers.AsciiDoc](http://hackage.haskell.org/package/pandoc-2.7.3/docs/Text-Pandoc-Writers-AsciiDoc.html)

## ORM and Database Connectors

* [*Which type-safe database library should you use?*
  ](https://williamyaoh.com/posts/2019-12-14-typesafe-db-libraries.html)
  2019-12 William Yao

### opaleye

* [*Opaleye Tutorial*
  ](https://www.haskelltutorials.com/opaleye/index.html)

### [esqueleto](https://www.stackage.org/package/esqueleto)
Type-safe EDSL for SQL queries, inspired by Scala's Squeryl closely resembles SQL.
Currently, SELECTs, UPDATEs, INSERTs and DELETEs are supported.

In particular, esqueleto is the recommended library for type-safe JOINs on 
persistent SQL backends. (The alternative is using raw SQL, but that's
error prone and does not offer any composability.
* 3458 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/esqueleto)
  ](https://hackage.haskell.org/package/esqueleto)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/esqueleto)
  ](https://libraries.io/hackage/esqueleto)
  [![GitHub All Releases](https://img.shields.io/github/downloads/bitemyapp/esqueleto/total)
  ](https://github.com/bitemyapp/esqueleto)
* persistent

### [persistent](https://www.stackage.org/package/persistent)
* 2383 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/persistent)
  ](https://hackage.haskell.org/package/persistent)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/persistent)
  ](https://libraries.io/hackage/persistent)
  [![GitHub All Releases](https://img.shields.io/github/downloads/yesodweb/persistent/total)
  ](https://github.com/yesodweb/persistent)
* persistent-postgresql: postgresql-simple, postgresql-libpq
* persistent-sqlite
* persistent-mysql: mysql-simple, mysql

### [hasql](https://www.stackage.org/package/hasql): An efficient PostgreSQL driver with a flexible mapping API
* 916 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/hasql)
  ](https://hackage.haskell.org/package/hasql)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/hasql)
  ](https://libraries.io/hackage/hasql)
  [![GitHub All Releases](https://img.shields.io/github/downloads/nikita-volkov/hasql/total)
  ](https://github.com/nikita-volkov/hasql)
* postgresql-binary and postgresql-libpq

### [groundhog](https://www.stackage.org/package/groundhog)
* 730 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/groundhog)
  ](https://hackage.haskell.org/package/groundhog)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/groundhog)
  ](https://libraries.io/hackage/groundhog)
  [![GitHub All Releases](https://img.shields.io/github/downloads/lykahb/groundhog/total)
  ](https://github.com/lykahb/groundhog)
* groundhog-sqlite: direct-sqlite
* groundhog-postgresql: postgresql-simple, postgresql-libpq
* groundhog-mysql: mysql-simple, mysql

### [HDBC](https://www.stackage.org/package/HDBC)
* 554 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/HDBC)
  ](https://hackage.haskell.org/package/HDBC)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/HDBC)
  ](https://libraries.io/hackage/HDBC)
  [![GitHub All Releases](https://img.shields.io/github/downloads/hdbc/hdbc/total)
  ](https://github.com/hdbc/hdbc)
* HDBC-mysql
* persistable-types-HDBC-pg
* postgresql-typed: postgresql-binary
* HDBC-postgresql (not in Stackage)
* HDBC-sqlite3 (not in Stackage)
* (HDBC-session)
* (yeshql-hdbc)

### HaskellDB (Not in Stackage 2019-12)
* 265 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/haskelldb)
  ](https://hackage.haskell.org/package/haskelldb)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/haskelldb)
  ](https://libraries.io/hackage/haskelldb)
  [![GitHub All Releases](https://img.shields.io/github/downloads/m4dc4p/haskelldb/total)
  ](https://github.com/m4dc4p/haskelldb)
* haskelldb-hsql: haskelldb-hsql-sqlite3, haskelldb-hdbc-postgresql, haskelldb-hsql-mysql...

### [Beam](https://www.stackage.org/package/beam-core)
* 233 downloads in the last 30 days (2019-12)
  [![Hackage-Deps](https://img.shields.io/hackage-deps/v/beam-core)
  ](https://hackage.haskell.org/package/beam-core)
  [![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/hackage/beam-core)
  ](https://libraries.io/hackage/beam-core)
  [![GitHub All Releases](https://img.shields.io/github/downloads/tathougies/beam/total)
  ](https://github.com/tathougies/beam)
* beam-postgres: postgresql-simple, postgresql-libpq
* beam-sqlite: sqlite-simple, direct-sqlite
* beam-mysql: mysql

## Database utilities
### Paginators
* [haskell paginator](https://google.com/search?q=haskell+paginator)

#### persistent-pagination
* [persistent-pagination](https://hackage.haskell.org/package/persistent-pagination)
* 109 downloads in the last 30 days (2020-01)

## Functional Reactive Programming (FRP)
* [Category:FRP](https://wiki.haskell.org/Category:FRP)
  wiki.haskell.org
  * [*Functional Reactive Programming*
    ](https://wiki.haskell.org/Functional_Reactive_Programming)
* [*FRP Zoo*](https://github.com/gelisam/frp-zoo)
  (2017)
  * Comparing many FRP implementations by reimplementing the same toy app in each.
* [*Functional reactive programming*
  ](https://www.worldcat.org/search?q=ti%3AFunctional+reactive+programming)
  2016-07 Stephen Blackheath and Anthony Jones
* [*Haskell High Performance Programming*
  ](https://www.worldcat.org/search?q=ti%3AHaskell+High+Performance+Programming)
  2016 Samuli Thomasson
  * Book with a chapter about Haskell main FRP libraries
* [*FRP made simple!*
  ](http://travis.athougies.net/posts/2015-05-05-frp-made-simple.html) in haskell
  2015-05 by Travis Athougies
  * Explain how to build a simple FRP library.
* [*Functional Reactive Animation*
  ](http://conal.net/papers/icfp97/)
  Appeared in ICFP 1997 Conal Elliott and Paul Hudak

### Libraries
* [*Functional reactive programming*
  ](https://en.m.wikipedia.org/wiki/Functional_reactive_programming)
  Section Implementations WikipediA
* [What are the best Functional Reactive Programming (FRP) libraries for Haskell?
  ](https://www.slant.co/topics/2349/~best-functional-reactive-programming-frp-libraries-for-haskell)
  Slant
* [Packages tagged frp
  ](https://hackage.haskell.org/packages/tag/frp)
---
![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libghc-active-dev+libghc-reactive-banana-dev+libghc-netwire-dev&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* 200 active (abstraction for animated things with finite start and end times [Not FRP], Debian)
* 128 Yampa https://wiki.haskell.org/Yampa
* 115 rhine
* 108 reflex [reflex-frp.org](https://reflex-frp.org)
  * [reflex-dom](https://hackage.haskell.org/package/reflex-dom)
  * [*Creating a Haskell Application Using Reflex*
    ](https://typeable.io/blog/2021-03-15-reflex-1)
    2021-03 Nikita Anisimov (Typeable)
* 085 reactive-banana (Debian), has Wx and Gtk connectors
  * [*Reactive-banana*](https://wiki.haskell.org/Reactive-banana)
    wiki.haskell.org
  * [*FRP explanation using reactive-banana*
    ](https://wiki.haskell.org/FRP_explanation_using_reactive-banana)
    wiki.haskell.org
* 076 netwire (Debian)
* 071 dunai: Generalised reactive framework supporting classic, arrowized and monadic FRP.
* 063 bearriver (FRP Yampa replacement implemented with Monadic Stream Functions)
* 059 varying
* 047 reactive
* 043 elerea

## Web Backends

* [*A dead-simple web stack in Haskell*
  ](https://williamyaoh.com/posts/2019-11-16-a-dead-simple-web-stack.html)
  2019-11 William Yao

### servant
### wai
### EulerHS: full-fledged framework for creating web backends
* https://github.com/juspay/euler-hs

### Hydra: full-fledged framework
* https://github.com/graninas/Hydra

# Packages
* [Stackage](https://www.stackage.org/)
* [Packages overview for Debian Haskell Group](https://qa.debian.org/developer.php?login=pkg-haskell-maintainers%40lists.alioth.debian.org)

# Tools
* [*Easy Haskell Development Setup with Docker*
  ](https://thoughtbot.com/blog/easy-haskell-development-and-deployment-with-docker)
  2019-03, 2015-07 Tony DiPasquale

## cabal or stack
* [*Why Not Both?
Build Haskell projects with either cabal or stack*
  ](https://medium.com/@fommil/why-not-both-8adadb71a5ed)
  2018-12 fommil

## Stack
* [*ghcup*](https://www.haskell.org/ghcup), installer for the general purpose language Haskell 
* [*The Haskell Tool Stack*](https://haskellstack.org)
  * [*Install/upgrade*](https://docs.haskellstack.org/en/stable/install_and_upgrade/)
* [*Building projects with stack](https://livebook.manning.com/book/get-programming-with-haskell/chapter-35/)
  Chapter 35 of Get Programming with Haskell
  * *Get Programming with Haskell*
    2018-03 Will Kurt (Manning)
* Cabal may help install Stack

## Static Analysis
* [Haskell Static Analysis Tools
  ](https://www.analysis-tools.dev/tag/haskell)
* [Stan](https://kowainik.github.io/projects/stan)

## Lint: hlint
* May be used by some text editors.
* Has also a command line interface.
* Available as a package in some common Linux distributions.

## Nix
* Nixpkgs Manual: [*Haskell*
  ](https://nixos.org/manual/nixpkgs/stable/#haskell)
  (2023)
* Nix Reference Manual: [*nix-shell*
  ](https://nixos.org/manual/nix/stable/command-ref/nix-shell.html)
  (2023)
* NixOS Wiki: [*Haskell*
  ](https://nixos.wiki/wiki/Haskell)
  (2023)
* Nixpkgs User’s Guide: [*How to install Haskell packages*
  ](https://haskell4nix.readthedocs.io/nixpkgs-users-guide.html)
  (2023)
* [*Nix recipes for Haskellers*
  ](https://srid.ca/haskell-nix)
  (2023)
* [*pkgs.dockerTools*
  ](http://ryantm.github.io/nixpkgs/builders/images/dockertools/)
  (2023)
* [*How to Setup a Haskell Programming Environment with Nix*
  ](https://lambdablob.com/posts/nix-haskell-programming-environment/)
  (2023)
* [*Getting Started With NixOS*
  ](https://itsfoss.com/nixos-tutorials/)
  2023-04 Abhishek Prakash (IT'S FOSS)
* [*Super-Simple Haskell Development with Nix*
  ](https://discourse.nixos.org/t/super-simple-haskell-development-with-nix/14287)
  2021 mhwombat (discourse.nixos.org)
* [*Super-Simple Haskell Development with Nix*
  ](https://github.com/mhwombat/nix-for-numbskulls/blob/78bcc186f79931c0e4a1e445e2f6b1f12f6d46be/Haskell/ss-haskell-dev.md)
  2021 mhwombat (GitHub)
* [*What’s all the hype with Nix?*
  ](https://discourse.haskell.org/t/whats-all-the-hype-with-nix/2593)
  2021 (discourse.haskell.org)

# Implementations
## GHC
* [*The Glasgow Haskell Compiler*](https://www.haskell.org/ghc/)
* [Try it online: tio.run](https://tio.run/#haskell)

### Extensions
* [*Language extensions*
  ](https://ghc.gitlab.haskell.org/ghc/doc/users_guide/exts.html)
* [*No implicit Prelude*
  ](https://typeclasses.com/ghc/no-implicit-prelude)
* `{-# LANGUAGE StrictData #-}`

### Warnings
* [*Enable All The Warnings*
  ](https://medium.com/mercury-bank/enable-all-the-warnings-a0517bc081c3)
  2018-08 Max Tagher

## Javascript
* [purescript](https://hackage.haskell.org/package/purescript)
  : PureScript Programming Language Compiler
  * A small strongly, statically typed programming language with expressive types,
    inspired by Haskell and compiling to JavaScript

# Other languages compared to Haskell
* [*Difference Between Haskell and Scala*
  ](https://www.geeksforgeeks.org/differnece-between-haskell-and-scala/)
  2021-08 miniyadav1
* [*When Rust is safer than Haskell*
  ](https://www.fpcomplete.com/blog/when-rust-is-safer-than-haskell/)
  2019-01 Michael Snoyman
